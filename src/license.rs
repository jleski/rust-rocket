use serde_derive::{Serialize, Deserialize};
use diesel;
use diesel::prelude::*;
use diesel::mysql::MysqlConnection;
extern crate serde;
extern crate serde_derive;
extern crate serde_json;
use crate::schema::licenses;

#[derive(Queryable, Identifiable, AsChangeset, Serialize, Deserialize)]
#[table_name = "licenses"]
pub struct License {
    pub id: i32,
    pub license_type: i32
}

#[derive(Insertable, AsChangeset, Serialize, Deserialize)]
#[table_name = "licenses"]
pub struct NewLicense {
    pub license_type: i32
}


impl NewLicense {
    pub fn create(license: NewLicense, connection: &MysqlConnection) -> License {
        diesel::insert_into(licenses::table)
            .values(&license)
            .execute(connection)
            .expect("Error creating new license");

        licenses::table.order(licenses::id.desc()).first(connection).unwrap()
    }
}

impl License {
    pub fn read_one(id: i32, connection: &MysqlConnection) -> License {
        licenses::table
            .find(id)
            .first::<License>(connection)
            .unwrap()
    }
    pub fn read(connection: &MysqlConnection) -> Vec<License> {
        licenses::table
            .order(licenses::id)
            .load::<License>(connection)
            .unwrap()
    }
    pub fn update(id: i32, license: License, connection: &MysqlConnection) -> bool {
        diesel::update(
            licenses::table
                .find(id)
            )
            .set(&license)
            .execute(connection)
            .is_ok()
    }
    pub fn delete(id: i32, connection: &MysqlConnection) -> bool {
        diesel::delete(
            licenses::table
                .find(id)
            )
            .execute(connection)
            .is_ok()
    }
}

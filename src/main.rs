#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate diesel;
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
extern crate serde_derive;
extern crate r2d2;
extern crate r2d2_diesel;


mod license;
mod db;
#[macro_use] mod schema;
use rocket_contrib::json::{Json, JsonValue};
use crate::license::{License, NewLicense};
use dotenv::dotenv;
use std::alloc::System;
#[global_allocator] static A: System = System;


// TODO: Move routes to module
// TODO: Move main logic to separate module 
// TODO: Write some integration tests using the main module

//GET /
#[get("/")]
fn index() -> &'static str {
    "Nothing to see here"
}

//GET /ping
#[get("/")]
fn pong() -> &'static str {
    "pong!"
}


//GET /licenses
#[get("/")]
fn read(conn: db::Connection) -> Json<JsonValue> {
    Json(json!(License::read(&conn)))
}

//POST /license
#[post("/", data = "<license>")]
fn create(license: Json<NewLicense>, conn: db::Connection) -> Json<License> {
    let insert = NewLicense { ..license.into_inner() };
    Json(NewLicense::create(insert, &conn))
}

//PUT /license/id
#[put("/<id>", data = "<license>")]
fn update(id: i32, license: Json<License>, conn: db::Connection) -> Json<JsonValue> {
    let update = License { id, ..license.into_inner() };
    Json(json!({
        "success": License::update(id, update, &conn)
    }))
}

//DELETE /license/id
#[delete("/<id>")]
fn delete(id: i32, conn: db::Connection) -> Json<JsonValue> {
    Json(json!({
        "success": License::delete(id, &conn)
    }))
}

//GET /license/id
#[get("/<id>")]
fn fetch(id: i32, conn: db::Connection) -> Json<JsonValue> {
    Json(json!(License::read_one(id, &conn)))
}


fn main() {
    dotenv().ok();
    rocket::ignite()
        .manage(db::connect())
        .mount("/", routes![index])
        .mount("/license", routes![create,update,delete,fetch])
        .mount("/licenses", routes![read])
        .mount("/ping", routes![pong])
        .launch();
}

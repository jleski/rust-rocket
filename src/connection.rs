//extern crate diesel;
//extern crate dotenv;
//extern crate r2d2;
use r2d2_diesel::ConnectionManager;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request, State};
use rocket::http::Status;

//pub mod license;
//pub mod schema;

use diesel::mysql::MysqlConnection;
use std::env;
use std::ops::Deref;
use dotenv::dotenv;

type Pool = r2d2::Pool<ConnectionManager<MysqlConnection>>;

pub struct DbConn(pub r2d2::PooledConnection<ConnectionManager<MysqlConnection>>);

pub fn init_pool() -> Pool {
    dotenv().ok();
    //let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<MysqlConnection>::new(database_url());
    Pool::new(manager).expect("db pool")
}

fn database_url() -> String {
    env::var("DATABASE_URL").expect("DATABASE_URL must be set")
}

impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<DbConn, Self::Error> {
        let pool = request.guard::<State<Pool>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ())),
        }
    }
}

impl Deref for DbConn {
    type Target = MysqlConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

# select build image
#FROM rustlang/rust:nightly as build
#FROM ekidd/rust-musl-builder:nightly as build
FROM clux/muslrust as build

# create a new empty shell project
RUN USER=root cargo new --bin bootstrap
WORKDIR /bootstrap

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
COPY ./src ./src

# dependencies
RUN apt-get update -qq && \
    apt-get install libmysqlclient-dev -y -q

# build for release
RUN rustup target install x86_64-unknown-linux-musl
RUN cargo rustc --release -- -C link-args=-static
RUN strip /bootstrap/target/x86_64-unknown-linux-musl/release/hello-rocket

# our final base
FROM scratch

# copy the build artifact from the build stage
COPY --from=build /bootstrap/target/x86_64-unknown-linux-musl/release/hello-rocket ./bootstrap

# expose the api http port
EXPOSE 8000

# set the startup command to run your binary
CMD ["./bootstrap"]

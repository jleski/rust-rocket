
# Tiny Rust and Rocket Microservice

This sample demonstrates how to run a tiny mock microservice with Rust and Rocket in just 1.22 MB Docker container.

```
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
jleskinen/rusty-rocket   8a3d80              89193caeff10        About an hour ago   1.22MB
jleskinen/rusty-rocket   latest              89193caeff10        About an hour ago   1.22MB  
```

## How to run?

**1: Build the container**

sh build.sh

**2: Run the container and expose port 8000**

docker run -it -p 8000:8000 --rm jleskinen/rusty-rocket:latest

**3: Test the service :)**

```
# invoke the GET /licenses endpoint function
curl http://localhost:8000/licenses

# invoke the POST /license endpoint function
curl -d '{"id": 1, "license_type": 999}' http://localhost:8000/license
```

## How did we get to 1.22MB?

By cargo compiling to release without debug symbols and with size optimizations and lto, building static binary against musl and finally stripping out unused symbols and sections. See Dockerfile and Cargo.toml for more reference.

## TODO

* Actual Terraform Deployment (figure out way to use pre-existing AWS VPC and subnets)

## DONE

* Build script using Terraform (deploys ECR, builds container image and pushes to ECR dynamically)

## Author

Jaakko Leskinen <<jaakko.leskinen@gmail.com>>